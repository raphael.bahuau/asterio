import Vector from "./vector";
import Hitbox from "./hitbox";

const get = (bound, hitbox, velocity, rotationSpeed = 0) => ({
  bound, // bounding box radius
  hitbox, // Circle or Polygon
  velocity, // Vector
  rotationSpeed, // radian

  forces: [], // accelerations (velocity) to apply at each UPS
  speedMax: velocity.magnitude, // speed limits
  drag: 1, // natural deceleration, default 1 = no natural deceleration
});

// True if bounding box entities touch each other
const boundCollides = (e1, e2) =>
  e1.hitbox.x - e1.bound < e2.hitbox.x + e2.bound &&
  e1.hitbox.y - e1.bound < e2.hitbox.y + e2.bound &&
  e1.hitbox.x + e1.bound > e2.hitbox.x - e2.bound &&
  e1.hitbox.y + e1.bound > e2.hitbox.y - e2.bound;

// Update orientation according to angular velocity
// - dt : deltaTime
const orientationUpdate = (e, dt) => ({
  ...e,
  hitbox: Hitbox.rotate(e.hitbox, e.velocity.angle * dt),
});

// Update position according to velocity
// - dt : deltaTime
const positionUpdate = (e, dt) => ({
  ...e,
  hitbox: Hitbox.move(e.hitbox, Vector.scale(e.velocity, dt)),
});

// Update speed by appying forces or drag
// - dt : deltaTime
const velocityUpdate = (e, dt) => {
  // If there is no forces => apply drag
  // -> Multiplicate by coef instead of calculate an opposite force of current speed %
  let wasForced = false;
  if (e.forces.length === 0) {
    e.velocity = Vector.scale(e.velocity, e.drag);
  }
  // Else add each force (multiplied by the dt) to the speed and clear the list
  else {
    e.forces = [];
    e.velocity = e.forces.reduce(
      (v, force) => Vector.sum(v, Vector.scale(force, dt)),
      e.velocity
    );
    // To prevent velocity set to 0 if there is at least one force
    wasForced = true;
  }
  // Correction according to speed max
  if (e.velocity.magnitude > e.speedMax) {
    e.velocity = Vector.fromQuantity(e.speedMax, e.velocity.direction);
  } else if (
    0 < e.velocity.magnitude &&
    e.velocity.magnitude < 5 &&
    !wasForced
  ) {
    e.velocity = Vector.fromQuantity(0, e.velocity.direction);
  }
  return e;
};

// Update speed then position then orientation
//  according to forces
// - dt (delta time) : to get moves in pixel/second
const stateUpdate = (e, dt) => {
  e = velocityUpdate(e, dt);
  e = positionUpdate(e, dt);
  e = orientationUpdate(e, dt);
  return e;
};

/* --------------------------------- EXPORT --------------------------------- */

const Entity = {
  get,
  boundCollides,
  stateUpdate,
};

export default Entity;
