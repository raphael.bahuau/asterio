'use strict';

module.exports = global.Serializable = class Serializable {
		// Return a json (litteral object) from a class object
		// - attrList : attributs to keep in json, ex : ('x', 'y')
		// 		if undefined => keep all attributs
		// MAYBE optimise the algo... or use a library
	static toJson (object, attrList) {
		if (typeof(object) !== 'object') {
			return object;
		}
		const json = {};
		for (let attr in object) {
			if (!attrList || attrList.includes(attr)) {
					// If the attribut value is an object
					// 	apply toJson() on each of his attributs
				if (typeof(object[attr]) === 'object') {
					if (object[attr] instanceof Array) {
							// MAYBE add attributs 'classIn' if this is an array
							//  to make understand at the setFromJson() function
							//  the class of the objects in the array
						json[attr] = [];
						if (object[attr].length > 0) {
							// json[attr].classIn = object[attr][0].constructor.name;
							for (let i in object[attr]) {
								json[attr][i] = Serializable.toJson(object[attr][i]);
							}
						}
					} else {
							// Litteral object
						json[attr] = Serializable.toJson(object[attr]);
					}
				}
					// Else if the attribut value is a native type, simple copy
				else {
					json[attr] = object[attr];
				}
			}
		}
		return json;
	}
		// Return a json string from a class object
	static toJsonString (object, attrList) {
		return JSON.stringify(Serializable.toJson(object, attrList));
	}

		// Return a class object from a json
		// - classRef : the class constructor
		// MAYBE not use classRef by put in the json directly
		// 	see globalThis['Class'] to instanciate with a string
		//	seem not work...
	static newFromJson (classRef, json) {
		const object = new classRef();
		Serializable.setFromJson(object, json);
		return object;
	}
		// Return a class object from a json string
	static newFromJsonString (classRef, json) {
		return Serializable.newFromJson(classRef, JSON.parse(json));
	}

		// Apply json data to a class object (ex Point)
	static setFromJson (object, json) {
		for (let attr in json) {
				// If attribut value is an array, for each value :
			if (object[attr] instanceof Array) {
				for (let i in json[attr]) {
					if (i === 'classIn') {
						continue;
					}
						// If the classIn attribut exist on json[attr]
						// 	instanciate a new class object with newFromJson()
					if (object[attr].classIn) {
						// console.log(globalThis[json[attr].classIn]);
						const classRef = globalThis[object[attr].classIn]
						object[attr][i] = Serializable.newFromJson(
							classRef, json[attr][i]);
					}
						// Else copy value
					else {
						object[attr][i] = json[attr][i];
					}
				}
			}
				// Else if the value is an object
			else if (typeof(object[attr]) === 'object') {
				object[attr] = Serializable.newFromJson(
					object[attr].constructor, json[attr]);
			}
				// Else copy normally the value
			else {
				if (typeof(object[attr]) === 'undefined') {
//					console.log(object.constructor.name + ' : '
//						 + attr + ' = ' + json[attr]);
				}

				object[attr] = json[attr];
			}
		}
	}
	static setFromJsonString (object, json) {
		Serializable.setFromJson(object, JSON.parse(json));
	}
}