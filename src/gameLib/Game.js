'use strict';

	// Model
module.exports = global.Game = class Game {

		// MAYBE later add some params like game mode...
	constructor () {
			// Space
		this.space = new Space();
		
			// Active entities
		this.entityManager = new EntityManager();

			// Game time in milliseconds : initialized in start()
		this.startTime
		this.previousTime;
		this.time;

			// communication Server / Client with messages
		this.messageManager = new MessageManager();
		this.isServer = false;
		this.isSolo = false;
	}
		// Set socket manager to send and receive messages
		// - socketManager : AbstractSocketManager = Client or Server
	setSocketManager (socketManager) {
			// Client game side network by default, init in setSocketManager()
		this.socketManager = socketManager;
			// If is Server
		if (socketManager instanceof Server) {
			this.isServer = true;
		}
			// Else if is clientSolo
		else if (socketManager instanceof ClientSolo) {
			this.isSolo = true;
		}
	}
		// Start game : /!\ use only after MVC associations
	start () {
			// Initialize time variable
		this.time = Date.now();
		console.log('Game startTime ' + this.time);

			// Set game loop every UPS (Updates Per Second)
		setInterval(() => {
				// Store previous time
			this.previousTime = this.time;
				// Set current time
			this.time = Date.now();
				// Delta between previous and current time
			this.frameTime = this.time - this.previousTime;
				// Update game with this delta (converted in second)
			this.update(this.frameTime / 1000);
		}, Game.MS_UPS);
	}
		// Engine called each UPS
	update (dt) {
		// --- Game mechanics
			// Collisions
		if (this.isServer || this.isSolo) {
			this.collisionUpdate(dt);
		}
			// Destructions
		this.removeUpdate();
			// Moves
		this.stateUpdate(dt);
			// Generations
		this.generateUpdate();

		// --- Network mechanics
			// Input
		this.inputUpdate();
			// Sort messages
		this.messageManager.setProcess();
		// this.messageManager.setProcess(this.time);


		// FIXME remove after testing
		// --- Debug
		if (this.isServer) {
			for (let [id, player] of this.entityManager.get(Player)) {
				if (this.debug !== parseInt(player.hitbox.x) &&
					player.velocity.magnitude === 0) {
					this.debug = parseInt(player.hitbox.x);
					// console.log(this.debug);
				}
				break;
			}
		}
	}

		// Apply interactions received from clients
	inputUpdate () {
		if (this.isServer) {
			for (let message of this.messageManager.inProcess
				[Message.Type.INPUT]) {
					// Get the player
				const player = this.entityManager.idMap.get(message.args.id);
					// Continue if the player is unidentifier
				if (!player) {
					continue;
				}
					// Apply interaction
				const keyCode = player.input.keyCode[message.args.keyName];
				switch (message.subType) {
					case Message.Input.KEY_DOWN :
						player.input.setKeyActivation(keyCode, true); break;
					case Message.Input.KEY_UP :
						player.input.setKeyActivation(keyCode, false); break;
					case Message.Input.MOUSE_MOVE :
						player.input.cursor.setPosition(
							message.args.x, message.args.y);
						player.input.cursor.setActivation(true); break;
					case Message.Input.MOUSE_DOWN :
							// CSP Calculate the input delta time, multiply by 2 to client prediction
							//	this work for 1 client, but if there is 2 clients with differents ping ?
							//	maybe it's okay, just need to properly display on the predicted client
						// const inputDt = (this.time - message.args.t) * 2;
						// player.input.cursor.setDown(true, inputDt); break;
						player.input.cursor.setDown(true); break;
					case Message.Input.MOUSE_UP :
						player.input.cursor.setDown(false); break;
				}
					// Broadcast interaction
				// this.messageManager.toSend.push(m);
			}
				// Clean message list
			this.messageManager.inProcess[Message.Type.INPUT] = [];
		}
	}

		// Generations
	generateUpdate () {
			// If Server or Solo : generate normally
		if (this.isServer || this.isSolo) {
			this.setUpBlackHoleGenerate();
			this.setUpAsteroidGenerate();
			this.setUpBulletGenerate();
		}
			// If Client side : generate from messages
		else {
			for (let message of this.messageManager.inProcess
				[Message.Type.STATE][Message.State.GENERATE]) {
				let entity;
				const json = message.args.json;
					// FIXME problem with variable scope in switch case
					// 	adding {...} to quickly fix it
				switch (message.args.type) {
					case Player.prototype.constructor.name : {
						entity = new Player(json.colorId);
						entity.hitbox = Serializable.newFromJson(
							Polygon, json.hitbox);
							// Défini la vue sur le joueur
						if (json.id === this.socketManager.id && 
							View.WATCH_PLAYER) {
							this.socketManager.view.watch(entity);
						}
						break;
					}
					case Bullet.prototype.constructor.name : {
						const player = this.entityManager.idMap.get(
							json.playerId);
						const shootOrigin = Serializable.newFromJson(
							Point, json.shootOrigin);
						const velocity = Serializable.newFromJson(
							Vector, json.velocity);
						entity = new Bullet(
							json.playerId, player.colorId, shootOrigin, 
							velocity, json.type);
						break;
					}
					case Asteroid.prototype.constructor.name : {
						const hitbox = Serializable.newFromJson(
							Polygon, json.hitbox);
						const velocity = Serializable.newFromJson(
							Vector, json.velocity);
						entity = new Asteroid(hitbox, velocity,
							json.rotationSpeed, json.size, json.first,
							json.gunConfig);
						break;
					}
					case BlackHole.prototype.constructor.name : {
						const velocity = Serializable.newFromJson(
							Vector, json.velocity);
						entity = new BlackHole(0, 0, velocity,
							json.lifeTime, json.finalRadius);
						entity.hitbox = Serializable.newFromJson(
							Circle, json.hitbox);
						Serializable.setFromJson(entity, json);
					}
				}
				entity.id = json.id;

				this.entityManager.toGenerate.push(entity);
			}
				// Clean message list
			this.messageManager.inProcess
				[Message.Type.STATE][Message.State.GENERATE] = [];
		}

		this.entityManager.handleGenerate();
	}

 		// Moves
 	stateUpdate (dt) {
			// If server or solo, calculate new positions etc...
		for (let [id, entity] of this.entityManager.idMap) {
			entity.stateUpdate(dt);
		}
		if (this.isServer || this.isSolo) {
			for (let [id, entity] of this.entityManager.idMap) {
					// If this is server, prepare update message
				if (this.isServer) {
					let attrList = ['hitbox', 'velocity'];
					if (entity.constructor.name ===
						BlackHole.prototype.constructor.name) {
						attrList.push('gravityRadius', 'coreRadius',
						'isGrowing', 'isExtinct');
					}
					// TODO add animationId in attr for player
					const message = new Message(
						Message.Type.STATE,
						Message.State.UPDATE,
						{
							id : id,
							json : Serializable.toJson(entity, attrList)
							// t : this.time
						}
					);
					this.messageManager.toSend.push(message);
				}
			}
				// Send and clean message list
			if (this.isServer) {
				this.socketManager.sendMessageList(this.messageManager.toSend);
				this.messageManager.toSend = [];
			}
		}
			// Else if is client : update with messages from server
		else  {
			// Update all hitboxes
			for (let message of this.messageManager.inProcess
				[Message.Type.STATE][Message.State.UPDATE]) {
				const entity = this.entityManager.idMap.get(message.args.id);
				if (entity) {
					Serializable.setFromJson(entity, message.args.json);
				}
			}
				// Clean list
			this.messageManager.inProcess
				[Message.Type.STATE][Message.State.UPDATE] = [];
		}
	 }
	 
		// Destructions
	removeUpdate () {
			// Server side or Solo
		if (this.isServer || this.isSolo) {
				// Check if blackhole or bullet should be removed
			for (let [id, entity] of this.entityManager.get(BlackHole, Bullet)) {
				if (entity.isExtinct) {
					this.setUpEntityRemove(id);
				}
			}
		}
			// Client side
		else {
			for (let message of this.messageManager.inProcess
				[Message.Type.STATE][Message.State.REMOVE]) {
					// Setup the destruction & animation
				this.setUpEntityRemove(message.args.id, message.args.animationId);
			}
			this.messageManager.inProcess
				[Message.Type.STATE][Message.State.REMOVE] = [];
		}
			// Process remove
		this.entityManager.handleRemove();
	}

		// Collisions
	collisionUpdate (dt) {
			// Reinitialize collision marker
		for (let [id, entity] of this.entityManager.get()) {
			entity.isInBoundCollision 	= false;
			entity.isInCollision 		= false;
		}
		//--- Space border
			// Asteroids & Bullets : destroy them
		for (let [id, entity] of this.entityManager.get(Asteroid, Bullet)) {
			if (!this.space.collision(entity)) {
				this.setUpEntityRemove(id);
			}
		}
			// BlackHoles : apply opposite force to keep them in Space
		for (let [id, blackHole] of this.entityManager.get(BlackHole)) {
			if (this.space.hitbox.range(blackHole.hitbox) 
				+ blackHole.hitbox.radius > this.space.coreRadius) {
				const angle = blackHole.hitbox.angle(this.space.hitbox);
				const force = new Vector(BlackHole.SPEED_MAX, angle);
				blackHole.addForce(force);
			}
		}
			// Players : apply opposite force to keep them in Space
		for (let [id, player] of this.entityManager.get(Player)) {
			if (this.space.isInGravityRadius(player)) {
				this.space.applyForce(player, dt);
			}
		}

		//--- BlackHoles
		for (let [id, blackHole] of this.entityManager.get(BlackHole)) {
				// Players & Asteroids : apply force to repulse them
			for (let [id2, entity] of this.entityManager.get(
				Player, Asteroid)) {
				if (blackHole.isInGravityRadius(entity)) {
				// console.log(blackHole.isInGravityRadius(entity));
				// if (blackHole.collision(entity)) {
					blackHole.applyForce(entity, dt);
				}
			}
				// Bullets : destroy them
			for (let [id2, bullet] of this.entityManager.get(Bullet)) {
				if (blackHole.isInCoreRadius(bullet.hitbox)) {
					this.setUpEntityRemove(id2);
				}
			}
				// Trou Noir : apply forces to repulse them each other
			for (let [id2, blackHole2] of this.entityManager.get(BlackHole)) {
				if (id !== id2 && blackHole.collision(blackHole2)) {
					const angle = blackHole2.hitbox.angle(blackHole.hitbox);
					const force = new Vector(BlackHole.SPEED_MAX, angle);
					blackHole.addForce(force);
				}
			}
		}

		//--- Asteroids
		for (let [id, asteroid] of this.entityManager.get(Asteroid)) {
				// MAYBE check collision between 2 Asteroids
				// 	this really slow down the game !
			for (let [id2, asteroid2] of this.entityManager.get(Asteroid)) {
				if (id !== id2 && asteroid2.boundCollision(asteroid)) {
					asteroid2.isInBoundCollision = true;
					asteroid.isInBoundCollision = true;
					if (asteroid2.collision(asteroid)) {
						asteroid2.isInCollision = true;
						asteroid.isInCollision = true;
					}
				}
			}
				// Players : hit them to make them lose score and bonus
				//  and destroy the hitting Asteroid
			for (let [id2, player] of this.entityManager.get(Player)) {
				if (!asteroid.isSplitting 
					&& player.boundCollision(asteroid)) {
					player.isInBoundCollision = true;
					asteroid.isInBoundCollision = true;
					if (player.collision(asteroid)) {
						player.isInCollision = true;
						asteroid.isInCollision = true;
						if (Game.PLAYER_LOSE_GUN) {
							player.gun.setConfig(GunConfig.NORMAL);
							if (this.isServer) {
								const message = new Message(
									Message.Type.STATE,
									Message.State.UPDATE,
									{
										id : id2,
										json : Serializable.toJson(player, ['gun'])
									}
								)
								this.messageManager.toSend.push(message);
							}
						}
						this.setUpEntityRemove(id);
					}
				}
			}
		}

		//--- Bullets
		for (let [id, bullet] of this.entityManager.get(Bullet)) {
				// Player : hit them to make them lose score
				//  and destroy the hitting Bullet
			for (let [id2, player] of this.entityManager.get(Player)) {
				if (bullet.playerId !== id2 && !bullet.isHitting
				 && bullet.boundCollision(player)) {
					bullet.isInBoundCollision = true;
					player.isInBoundCollision = true;
					if (player.contains(bullet.hitbox)) {
						bullet.isInCollision = true;
						player.isInCollision = true;
						this.setUpEntityRemove(id);
							// If FRAG Bullet => generate other Bullets
						for (let newBullet of bullet.hit(player.hitbox)) {
							this.setUpEntityGenerate(newBullet);
						}
						// TODO score
					//	const owner = 
					//		this.entityManager.get().get(bullet.playerId);
					}
				}
			}
				// Asteroids : hit them to make them split
				//  and destroy the hitting Bullet
			for (let [id2, asteroid] of this.entityManager.get(Asteroid)) {
				if (!bullet.isHitting && !asteroid.isSplitting 
					&& bullet.boundCollision(asteroid)) {
					bullet.isInBoundCollision = true;
					asteroid.isInBoundCollision = true;
					if (asteroid.contains(bullet.hitbox)) {
						bullet.isInCollision = true;
						asteroid.isInCollision = true;
						this.setUpEntityRemove(id, Animation.BLAST);
						this.setUpEntityRemove(id2, Animation.ASTEROID_BREAK);
							// If FRAG Bullet => generate other Bullets
						for (let newBullet of bullet.hit(asteroid.hitbox)) {
							this.setUpEntityGenerate(newBullet);
						}
							// Asteroid splitting
						for (let newAsteroid of asteroid.split(bullet)) {
							this.setUpEntityGenerate(newAsteroid);
						}
							// If Asteroid was a bonus
							//  and Player hadn't already a bonus
							//  change the Player's gun
						const player = this.entityManager.idMap.get(
							bullet.playerId);
						if (player) {
							if (player.gun.isNormal() && asteroid.isBonus()) {
								const configId = GunConfig.findId(
									asteroid.gunConfig);
								player.gun.setConfig(configId);
								if (this.isServer) {
									const message = new Message(
										Message.Type.STATE,
										Message.State.UPDATE,
										{
											id : player.id,
											json : Serializable.toJson(player, ['gun'])
										}
									)
									this.messageManager.toSend.push(message);
								}
							}
							// TODO score
						}
					}
				}

					// If HOMING Bullet : apply force to reach an Asteroid
				if (bullet.type == Bullet.HOMING) {
						// Doesn't use bullet.hitbox.near because we use the
						// 	range value in calcul
					const player = this.entityManager.idMap.get(
						bullet.playerId);
					const range = bullet.hitbox.range(asteroid.hitbox);
					if (!bullet.hitbox.near(player.hitbox, 50)
						&& range < Bullet.HOMING_RANGE) {
							// set orientation for drawing
						const direction = bullet.hitbox.angle(asteroid.hitbox);
							// + range is > and + force is strong
						const speed = Math.pow(1500 / range, 2);
						const acceleration = speed / dt;
						const force = new Vector(acceleration, direction);
						bullet.addForce(force);
					}
				}
			}
		}

		//--- TODO ? collision Player to Player
		for (let [id, player] of this.entityManager.get(Player)) {
				// MAYBE check collision between 2 Players
				//	repulse them each other ?
			for (let [id2, player2] of this.entityManager.get(Player)) {
				if (id !== id2
					// && !player.isInCollision
					// && !player2.isInCollision
					&& player2.boundCollision(player)) {
					player2.isInBoundCollision = true;
					player.isInBoundCollision = true;
					if (player2.collision(player)) {
							// The one wo expulse
						player2.isInCollision = true;
							// The expulsed
						player.isInCollision = true;
						// Try some shit
						/*
						const magnitude = player.velocity.magnitude / 2;
						const magnitude2 = player2.velocity.magnitude / 2;
						const pushVelocity = new Vector(
							(magnitude + magnitude2) / dt,
							player2.velocity.direction);
						const dragVelocity = new Vector(
							-(magnitude + magnitude2) / dt,
							player2.velocity.direction)

						player.addForce(pushVelocity);
						player2.addForce(dragVelocity);
						*/
					}
				}
			}
		}
	}

		// MAYBE refactor
		// Override the EntityManager methods for the server side
	setUpEntityGenerate (entity) {
		const id = this.entityManager.setUpGenerate(entity);
			// If this is Server, set the generation messages to broadcast
		if (this.isServer) {
			if (entity.constructor.name !== 'Asteroid') {
				// console.log('+ ' + id + ' : ' + entity.constructor.name);
			}
			let attrList = [];
			
			switch (entity.constructor.name) {
				case Player.prototype.constructor.name :
					attrList = ['colorId', 'hitbox']; break;
				case Bullet.prototype.constructor.name :
					attrList = ['playerId', 'shootOrigin', 'type', 'velocity']; 
					break;
				case Asteroid.prototype.constructor.name :
					attrList = ['hitbox', 'velocity', 'rotationSpeed', 'size', 
						'first', 'gunConfig']; break;
				case BlackHole.prototype.constructor.name :
					attrList = ['hitbox', 'velocity', 'lifeTime',
						'finalRadius', 'time', 'growingTime', 'growingCoef',
						'isGrowing', 'isExtinct']; break;
			}
			attrList.push('id');

			const message = new Message(
				Message.Type.STATE,
				Message.State.GENERATE, 
				{
					type : entity.constructor.name,
					json : Serializable.toJson(entity, attrList)
					// t : this.time
				}
			);
			this.messageManager.toSend.push(message);
		}
		return id;
	}
		// MAYBE refactor
	setUpEntityRemove (id, animationId) {
		this.entityManager.setUpRemove(id);
		const entity = this.entityManager.idMap.get(id);
			// Broadcast message if is Server side or Solo
		if (this.isServer || this.isSolo) {
			if (entity.constructor.name !== 'Asteroid') {
				// console.log('- ' + id + ' : ' + entity.constructor.name);
			}

			const message = new Message(
				Message.Type.STATE,
				Message.State.REMOVE,
				{ 
					id : id,
					animationId : animationId
					// t : this.time
				}
			);
			this.messageManager.toSend.push(message);
		}
			// Setup animation if is Client side or Solo
		if ((!this.isServer || this.isSolo) && View.DRAW_ANIMATION) {
			// console.log(animationId);
			this.socketManager.view.setUpAnimation(entity, animationId);
		}
	}
		// Generate a new BlackHole (Server or Solo)
	setUpBlackHoleGenerate () {
		const blackHoleMap = this.entityManager.get(BlackHole);
		if (blackHoleMap.size < Game.NB_BLACKHOLE) {
				// Origin and radius
			const origin = this.space.newInnerPoint(
				BlackHole.RADIUS_MAX + Space.GRAVITY_RADIUS);
			const finalRadius = Utility.random(
				BlackHole.RADIUS_MIN, BlackHole.RADIUS_MAX);
				// If the new will touch a blackhole already generated, cancel
				//  the generation to avoid overlay collision problems
			for (let [id, blackHole] of blackHoleMap) {
				if (blackHole.hitbox.near(origin, finalRadius*2)) {
					return;
				}
			}
				// Speed and direction
			const speed = Utility.random(
				BlackHole.SPEED_MIN, BlackHole.SPEED_MAX);
			const direction = Utility.toRad(Utility.random(359));
			const velocity = new Vector(speed, direction);
			// const velocity = new Vector(0, direction);
				// Lifetime duration
			const lifeTime = Utility.random(
				BlackHole.MIN_LIFETIME, BlackHole.MAX_LIFETIME);
				// Finally the entity
			const blackHole = new BlackHole(
				// 0, 0, velocity, lifeTime, finalRadius);
				origin.x, origin.y, velocity, lifeTime, finalRadius);
			this.setUpEntityGenerate(blackHole);
		}
	}
		// Generate a new Asteroid
 	setUpAsteroidGenerate () {
			// Simple constant configuration, doesn't generate if freq is 0
		if (!Game.NB_ASTEROID) {
			return;
		}
			// Get current number of Asteroids on the map (Server or Solo)
		const nbAsteroid = this.entityManager.get(Asteroid).size;
		if (nbAsteroid < Game.NB_ASTEROID) {
				// Default apparition from Space border
				//  but can appear from blackhole (50%)
			let hole = this.space;
			if (Utility.random(1)) {
				const blackHoleList = this.entityManager.get(BlackHole);
				const blackHole = Utility.randomInMap(blackHoleList);
				if (blackHole) {
					hole = blackHole;
				}
			}
				// Define if this is a bonus
			const isBonus = (Game.BONUS_FREQ && !Utility.random(
				this.entityManager.nbBonus * Game.BONUS_FREQ));
				// Finally the entity
			const asteroid = Asteroid.newAsteroid(hole, isBonus);
			this.setUpEntityGenerate(asteroid);
		}
	}
		// Generate all new Bullets (1 for each Player if they are shooting)
		// 	(Server or Solo)
	setUpBulletGenerate () {
		for (let [id, player] of this.entityManager.get(Player)) {
			if (player.input.cursor.isDown && player.gun.isReloaded) {
				const bulletList = player.shoot();
				for (let bullet of bulletList) {
						// CSP Forward the position by input time
					// bullet.stateUpdate(player.input.cursor.inputDt/1000);
						// Then setup the generation and broadcast message
					this.setUpEntityGenerate(bullet);
				}
			}
		}
	}
}