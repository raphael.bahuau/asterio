'use strict';

require('./Point.js');

module.exports = global.Polygon = class Polygon extends Point {

		// List of points with origin center (for rotation)
		// - xList : list of point or simple float value
		// 		If arrays are empty, sould be filled after
		// - yList : same lenght as xList
		// - origin : Point
	constructor (xList = [], yList = [], origin = new Point()) {
		super(origin.x, origin.y);

		this.orientation = 0;

		this.pointList = [];
			// For serialization
		this.pointList.classIn = 'Point';

			// MAYBE useless ? Can pass point list directly with this
		// if (xList[0] instanceof Point) {
		// 	for (let point of xList) {
		// 		this.pointList.push(point);
		// 	}
		// } else {
			for (let i = 0; i < xList.length; i++) {
				const point = new Point(
					xList[i] + origin.x, yList[i] + origin.y);
				this.pointList.push(point);
			}
		// }
	}

	//--- Movable
		// Set x & y, they are the new origin position
	// @Override
	setPosition (x, y) {
			// Shift all points to match new position
		for (let point of this.pointList) {
			point.shift(x - this.x, y - this.y);
		}
			// Then set origin
		super.setPosition(x, y);
	}
		// Translation (in pixel)
		// - vx : Vector or Point
	// @Override
	shift (vx, vy) {
			// Shift all points
		for (let point of this.pointList) {
			point.shift(vx, vy);
		}
			// Then shift origin
		super.shift(vx, vy);
	}
		// Rotate by angle (in radian) around the origin in args (Point)
	// @Override
	pivot (origin, angle) {
		for (let point of this.pointList) {
			point.pivot(origin, angle);
		}
		super.pivot(origin, angle);
	}

	//--- Rotatable
		// Set orientation angle (in radian) by pivoting around this.origin
	setOrientation (orientation) {
		this.pivot(this, orientation - this.orientation);
		this.orientation = orientation % Math.PI2;
	}
		// Rotate by angle (in radian) arount this.origin
	rotate (angle) {
		this.setOrientation(this.orientation + angle);
	}

	//--- Hitbox
		// MAYBE try to optimise 1/2 point
		// 	on a 6 side polygon => 0 - 2 - 4 - 1 - 3 - 5
		// TRUE if the point is in the polygon
	contains (point) {
		for (let i = 0, l = this.pointList.length; i < l; i++) {
			const i2 = (i + 1) % l;
				// Vector between 2 points of the polygon
			const v1 = {
				x : this.pointList[i2].x - this.pointList[i].x,
				y : this.pointList[i2].y - this.pointList[i].y};
				// Vector between the point and a point of the polygon
			const v2 = {
				x : point.x - this.pointList[i].x,
				y : point.y - this.pointList[i].y};
				// Calculate if the point is on the right of the segment formed
				// 	by the 2 points of the vector 1
				//  If it's true, the point is obviously outside
			if (v1.x*v2.y - v1.y*v2.x < 0) {
				return false;
			}
		}
			// Else if the point is always on the left, he's inside
		return true;
	}
		// TRUE if the 2 polygon are touching each other
		//  (at least 1 point is in the other)
		// - hitbox : Polygon ou Circle
	// MAYBE for circle collision return the true collision point
	//	(calculate the perpendicular of the vector of the contains function)
	collision (hitbox) {
		for (let point of this.pointList) {
			if (hitbox.contains(point)) {
				return true;
			}
		}
		return false;
	}

	//--- Clonable
	// @Override
	clone () {
		const polygon = new Polygon();
		polygon.x = this.x;
		polygon.y = this.y;
		for (let point of this.pointList) {
			polygon.pointList.push(point.clone());
		}
		return polygon;
	}
		// Clone only origin
	cloneOrigin () {
		return super.clone();
	}

	//--- Drawable
		// A polygon
	// @Override
	draw (view, color = 'white', style = View.FILL, lineWidth = 1) {
		if (this.pointList.length > 0) {
			view.ctx.lineWidth = lineWidth;
			view.ctx[style + 'Style'] = color;
			view.ctx.beginPath();
			view.ctx.moveTo(
				this.pointList[0].x + view.x, 
				this.pointList[0].y + view.y);
			for (let point of this.pointList) {
				view.ctx.lineTo(point.x + view.x, point.y + view.y);
			}
			view.ctx.closePath();
			view.ctx[style]();
		}
	}
		// Draw only origin
	drawOrigin (view, color, radius, style, lineWidth) {
		super.draw(view, color, radius, style, lineWidth);
	}
}