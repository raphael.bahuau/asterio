'use strict';

	// "Bonus"
module.exports = global.GunConfig = class GunConfig {

		// - cadenceCoef : shoot/seconds
		// - bulletType : Bullet.NORMAL ...
		// - colorId : Utility.RED
	constructor (cadenceCoef, bulletType, colorId) {
		this.bulletSecond 	= Gun.CADENCE * cadenceCoef;
		this.bulletType		= bulletType;
		this.colorId		= colorId;

		this.reloadTime = 1 / this.bulletSecond;
	}

		// Find a gun config by id
	static findId (gunConfig) {
		for (let [id, config] of GunConfig.MAP) {
			if (config === gunConfig) {
				return id;
			}
		}
	}
}