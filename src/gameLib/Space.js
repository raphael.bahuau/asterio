'use strict';

module.exports = global.Space = class Space extends Hole {

	constructor () {
		super(0, 0, Space.CORE_RADIUS, Space.GRAVITY_RADIUS, false);
	}
		// Retourn a random point inside space
	newInnerPoint (minusRadius = 0) {
		const direction = Utility.toRad(Utility.random(359));
		const radius = Utility.random(this.hitbox.radius - minusRadius);
		const vector = new Vector(radius, direction);
		return new Point(vector.x, vector.y);
	}
		// Retourn a radom point on space border
	newBorderPoint () {
		const direction = Utility.toRad(Utility.random(359));
		const vector = new Vector(this.hitbox.radius, direction);
		return new Point(vector.x, vector.y);
	}
}