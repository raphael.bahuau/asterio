import Point from "./point";

// - x, y: 2D coordinates of the origin
// - points: the points array of the polygon
// - angle: orientation in radian, used for rotation
const get = (points, origin = Point.ZERO, angle = 0) => ({
  ...origin,
  points,
  angle,
});

// Move the points and the origin
const move = (polygon, vector) => ({
  ...polygon,
  ...Point.sum(polygon, vector),
  points: polygon.points.map((p) => Point.sum(p, vector)),
});

// Set the origin and move the points to match the new position arround origin
// - origin: Point
const setPosition = (polygon, origin) =>
  move(polygon, Point.sub(origin, polygon));

// Rotate the points arount the origin
// - angle: radian
const rotate = (polygon, angle) => ({
  ...polygon,
  angle: (polygon.angle + angle) % (Math.PI * 2),
  points: polygon.points.map((p) => Point.pivot(p, polygon, angle)),
});

// Set the orientation and move the points to match the angle arround origin
// - angle: radian
const setAngle = (polygon, angle) => rotate(polygon, angle - polygon.angle);

/* --------------------------------- EXPORT --------------------------------- */

const Polygon = {
  get,
  move,
  setPosition,
  rotate,
  setAngle,
};

export default Polygon;
