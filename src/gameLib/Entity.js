'use strict';

	// Mobile object
module.exports = global.Entity = class Entity {

		// - bound : bounding box radius in pixel (square hitbox)
		// - hitbox : Hitbox (Circle or Polygon)
		// - velocity : Vector
		// - rotationSpeed : in angle/second
	constructor (bound, hitbox, velocity, rotationSpeed) {
		this.bound 			= bound || 0;
		this.hitbox			= hitbox || new Polygon();
		this.velocity		= velocity || new Vector();
		this.rotationSpeed 	= rotationSpeed || 0;;

			// MAYBE find an utility latter
		// this.mass = 1;

			// Forces list (accelerations) to apply at each UPS (in Vector)
		this.forceList = [];

			// The speed max is the original speed
		this.speedMax = this.velocity.magnitude;
			// Natural deceleration, default 1 = no natural deceleration
		this.drag = 1;

			// To interpolate for drawing
		this.previousHitbox = this.hitbox.clone();
		this.deltaVelocity 	= new Vector();
		this.deltaRotation 	= 0;

		this.isInCollision 		= false;
		this.isInBoundCollision = false;
	}

	//--- Movable
		// X & Y : set by shifting hitbox
	setPosition (x, y) {
		this.hitbox.shift(x - this.hitbox.x, y - this.hitbox.y);
	}
		// Shift in pixel
		// - vx : can be a Vector
	shift (vx, vy) {
		this.hitbox.shift(vx, vy);
	}
		// Rotate around origin
		// - origin : Point
		// - angle : in radian
	pivot (origin, angle) {
		this.hitbox.pivot(origin, angle);
	}

	//--- Rotatable
		// Orientation if entity is rotatable
	setOrientation (orientation) {
		if (this.hitbox.__proto__.hasOwnProperty('setOrientation')) {
			this.hitbox.setOrientation(orientation);			
		}
	}
		// Rotate the hitbox
	rotate (angle) {
		if (this.hitbox.__proto__.hasOwnProperty('rotate')) {
			this.hitbox.rotate(angle);
		}
	}

	//--- Hitbox
		// TRUE if the point is in hitbox
		// - point : Point
	contains (point) {
		return this.hitbox.contains(point);
	}
		// TRUE if hitbox entities touch each other
		// - entity : Entity
	collision (entity) {
		return this.hitbox.collision(entity.hitbox);
	}
		// True if bounding box entities touch each other
		// - entity : Entity
	boundCollision (entity) {
		if (this.hitbox.x - this.bound > entity.hitbox.x + entity.bound
			|| this.hitbox.y - this.bound > entity.hitbox.y + entity.bound
			|| this.hitbox.x + this.bound < entity.hitbox.x - entity.bound
			|| this.hitbox.y + this.bound < entity.hitbox.y - entity.bound){
			return false;
		}
		return true;
	}

	//--- Tangible
		// Add a force to apply on next game update
		// - force : Vector
	addForce (force) {
		this.forceList.push(force);
	}
		// Update speed then position then orientation
		//  according to forces
		// - dt (delta time) : to get moves in pixel/second
	stateUpdate (dt) {
			// Store current hitbox for ghost update (to interpolate for draw)
		this.previousHitbox = this.hitbox.clone();
			// Then update
		this.velocityUpdate(dt);
		this.positionUpdate(dt);
		this.orientationUpdate(dt);
	}
		// Update speed by appying forces or drag
		// - dt : deltaTime
	velocityUpdate (dt) {
			// If there is no forces => apply drag
			// -> Multiplicat by coef  par le coef instead of calculate an
			// an opposite force of current speed %
		let isForced = false;
		if (this.forceList.length === 0) {
			this.velocity.scale(this.drag);
		}
			// Else add each force (multiplied by the dt)
			//  to the speed and clear the list
		else {
			for (let force of this.forceList) {
				// const acceleration = (force.magnitude/this.mass) * dt;			
				const acceleration = force.magnitude * dt;
				const deltaForce = new Vector(acceleration, force.direction);
				this.velocity.sum(deltaForce);
			}
			this.forceList = [];
				// To prevent velocity set to 0 if there is at least one force
			isForced = true;
		}
			// Correction according to max speed
		if (this.velocity.magnitude > this.speedMax) {
			this.velocity.setMagnitude(this.speedMax);
		} else if (0 < this.velocity.magnitude && 
				this.velocity.magnitude < 5 && !isForced) {
			this.velocity.setMagnitude(0);
		}
	}
		// Update position according to speed
		// - dt : deltaTime
	positionUpdate (dt) {
		this.deltaVelocity = new Vector(
			this.velocity.magnitude * dt,
			this.velocity.direction);
		this.shift(this.deltaVelocity);
	}
		// Update orientation according to rotation speed
		// - dt : deltaTime
	orientationUpdate (dt) {
		this.deltaRotation = this.rotationSpeed * dt;
		this.rotate(this.deltaRotation);
	}

	//--- Drawable
		// Update ghost entity according to current hitbox and view delta time
		// - dt : view.deltaTime
	ghostUpdate (dt) {
		if (!this.ghost) {
			this.ghost = new Entity();
		}

		this.ghost.hitbox 			= this.previousHitbox.clone();
		this.ghost.velocity 		= this.deltaVelocity;
		this.ghost.rotationSpeed 	= this.deltaRotation;

		this.ghost.positionUpdate(dt);
		this.ghost.orientationUpdate(dt);
	}

		// Draw only hitbox and bounding box
	drawPhysic (view) {
		if (View.DRAW_BOUND || View.DRAW_HITBOX) {
				// Bounding box
			view.ctx.setLineDash([1, 4]);
			let color;
			if (this.isInCollision) {
				color = 'red';
			} else if (this.isInBoundCollision) {
				color = 'orange';
			} else {
				color = 'white';
			}
			if (View.DRAW_BOUND) {
				view.ctx.strokeStyle = color;
				view.ctx.strokeRect(
					this.hitbox.x - this.bound + view.x, 
					this.hitbox.y - this.bound + view.y,
					this.bound * 2,
					this.bound * 2);
			}
				// Hitbox
			if (View.DRAW_HITBOX) {
				this.hitbox.draw(view, color, View.STROKE);
			}
			view.ctx.setLineDash([]);
		}
	}
}