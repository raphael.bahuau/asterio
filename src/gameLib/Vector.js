'use strict';

module.exports = global.Vector = class Vector extends Point {

		// - magnitude : usually a distance or a force in pixel
		// - direction : angle in radian
		// - byPosition : TRUE => magnitude = x & direction = y
	constructor (magnitude, direction, byPosition) {
		if (!byPosition) {
			super();
			this.magnitude 	= magnitude	|| 0;
			this.direction 	= direction || 0;
			this.positionUpdate();
		} else {
			super(magnitude, direction);
			this.wayUpdate();
		}
	}
		// Set magnitude in pixel
	setMagnitude (magnitude) {
		this.magnitude = magnitude;
		this.positionUpdate();
	}
		// Add scalar to magnitude in pixel
	extend (scalar) {
		this.magnitude += scalar;
		this.positionUpdate();
	}
		// Multiply the magnitude in pixel
	scale (coef) {
		this.magnitude *= coef;
		this.positionUpdate();
	}
		// Sum 2 vector by shifting one by the other
	sum (vector) {
		this.shift(vector);
		this.wayUpdate();
	}
		// Update x & y
	positionUpdate () {
		this.x = this.magnitude*Math.cos(this.direction);
		this.y = this.magnitude*Math.sin(this.direction);
	}
		// Update magnitude and direction by x & y
	wayUpdate () {
		this.magnitude 	= this.range(Point.ZERO);
		this.direction 	= Point.ZERO.angle(this);
	}

	//--- Movable
		// Set x & y
	// @Override
	setPosition (x, y) {
		super.setPosition(x, y);
		this.wayUpdate();
	}
		// Translation (in pixel)
		// - vx : Vector or Point
	// @Override
	shift (vx, vy) {
		super.shift(vx, vy);
		this.wayUpdate();
	}
		// Rotate by angle (in radian) around the origin (Point)
	// @Override
	pivot (origin, angle) {
		super.pivot(origin, angle);
		this.wayUpdate();
	}

	//--- Rotatable
		// Set orientation angle (in radian) by pivoting around this x & y
	setOrientation (orientation) {
		this.direction = orientation;
		this.positionUpdate();
	}
		// Rotate by angle (in radian) arount this x & y
	rotate (angle) {
		this.direction = (this.direction + angle)%Math.PI2;
		this.positionUpdate();
	}

	//--- Clonable
	//@Override
	clone () {
		return new Vector(this.magnitude, this.direction);
	}

	//--- Drawable
		// origin : Point
	draw (view, origin) {
		if (View.DRAW_VELOCITY) {
			view.ctx.lineWidth = 1;
			view.ctx[View.STROKE + 'Style'] = 'lime';
			view.ctx.beginPath();
			view.ctx.moveTo(origin.x + view.x, origin.y + view.y);
			view.ctx.lineTo(origin.x + this.x*10 + view.x, origin.y + this.y*10 + view.y);
			view.ctx.closePath();
			view.ctx[View.STROKE]();
		}
	}
}