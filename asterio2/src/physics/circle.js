import Point from "./point";

const get = (x, y, radius) => ({ x, y, radius });

const move = (circle, vector) => ({
  ...circle,
  ...Point.sum(circle, vector),
});

/* --------------------------------- EXPORT --------------------------------- */

const Circle = {
  get,
  move,
};

export default Circle;
