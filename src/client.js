'use strict';

	// gameLib
const req = require.context('./gameLib', false, /\.js$/);
req.keys().forEach(function(key) {
	req(key);
});


//--- Images
Utility.SHIP_TILESET = new Image();
Utility.SHIP_TILESET.src = require('./img/ship_tileset.png');

Utility.BULLET_TILESET = new Image();
Utility.BULLET_TILESET.src = require('./img/bullet_tileset.png');


	// When windows loaded
window.onload = function () {

		// Server connection
	const socket = io.connect();

		// Model
	const game = new Game();

		// View
	const view = new View(game);

		// Controller
	const client = new Client(game, view, socket);

		// Start client side game
	game.start();
	view.start();

		// Globals for browser debugging
	global.Asterio = {
		game 	: game,
		client 	: client,
		view 	: view
	};
}
