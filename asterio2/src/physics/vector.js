import Point from "./point";

// Get position (x & y) from quantity (magnitude & direction)
// - magnitude: usually a distance or a force in pixel
// - direction: angle in radian
const fromQuantity = (magnitude, direction) => ({
  magnitude,
  direction,
  x: magnitude * Math.cos(direction),
  y: magnitude * Math.sin(direction),
});

// Get quantity (magnitude & direction) from position (x & y)
// - x, y: 2D coordinates
const fromPosition = (x, y) => ({
  x,
  y,
  magnitude: Point.range(Point.get(x, y), Point.ZERO),
  direction: Point.angle(Point.get(x, y), Point.ZERO),
});

const sum = (v1, v2) => fromPosition(...Point.sum(v1, v2));

const sub = (v1, v2) => fromPosition(...Point.sub(v1, v2));

// Add scalar to magnitude
const extend = (v, scalar) => fromQuantity(v.magnitude + scalar, v.direction);

// Multiply the magnitude
const scale = (v, coef) => fromQuantity(v.magnitude * coef, v.direction);

// Rotate around x & y
// - angle: radian
const rotate = (v, angle) =>
  fromQuantity(v.magnitude, (v.direction + angle) % (Math.PI * 2));

/* --------------------------------- EXPORT --------------------------------- */

const Vector = {
  fromQuantity,
  fromPosition,
  sum,
  sub,
  extend,
  scale,
  rotate,
};

export default Vector;
