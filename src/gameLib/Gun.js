'use strict';

module.exports = global.Gun = class Gun {

		// - configId : bonus id for tileset image
	constructor (configId = GunConfig.NORMAL) {

			// Gun config (normal, double, laser...)
		this.configId = configId;
		this.config;
		this.timeConfig;
		this.setConfig(configId);

			// TRUE when the gun can fire again
		this.isReloaded = true;

			// Time for reload
		this.currentReloadTime = 0;
	}

		// TRUE if is normal gun (for readability)
	isNormal () {
		return this.configId === GunConfig.NORMAL;
	}

		// Change the gun (when a player bullet hit a bonus asteroid)
	setConfig (configId) {
		this.configId = configId;
		this.config = GunConfig.MAP.get(configId);
			// Set time before the gun return to normal
		if (!this.isNormal()) {
			this.timeConfig = Gun.TIME_CONFIG;
		} else {
			this.timeConfig = 0;
		}
	}

		// Update time for bonus config and reload
	update (dt) {
		this.timeConfigUpdate(dt);
		this.reloadTimeUpdate(dt);
	}

		// Update time config
		// - dt : deltaTime (milliseconds)
	timeConfigUpdate (dt) {
		if (!this.isNormal() && !Game.PLAYER_KEEP_GUN) {
			this.timeConfig -= dt;
				// If the time is over, reset gun to normal config
			if (this.timeConfig <= 0) {
				this.setConfig(GunConfig.NORMAL);
			}
		}
	}

		// Update reload time
	reloadTimeUpdate (dt) {
		this.currentReloadTime += dt;
		if (this.currentReloadTime >= this.config.reloadTime) {
			this.isReloaded = true;
		}
	}

		// Reset reload time
	resetReloadTime () {
		this.currentReloadTime = 0;
		this.isReloaded = false;
	}

		// Return a bullets array
		// - player : the gun owner to link with his bullets
	shoot (player) {
		this.resetReloadTime();
		const bulletList = [];

		switch (this.configId) {

				// 1 bullet with a certain type
			case GunConfig.NORMAL 	:
			case GunConfig.FRAG 	:
			case GunConfig.HOMING	:
					// Classic bullet speed
				const velocity = new Vector(
					Bullet.SPEED, player.hitbox.orientation);
					// Origin from the ship center
				const origin = player.hitbox.cloneOrigin();

				const bullet = new Bullet(player.id, player.colorId,
					origin, velocity, this.config.bulletType);
				bulletList.push(bullet);
				break;
			
				// 2 bullets next to each other
			case GunConfig.DOUBLE :
				for (let i = -1; i < 2; i += 2) {
						// Classic bullet speed
					const velocity = new Vector(
						Bullet.SPEED, player.hitbox.orientation);
						// Origin from ship center offsetted
					const origin = player.hitbox.cloneOrigin();
					const offsetVector = new Vector(
						5, player.hitbox.orientation + Math.PI_2 * i);
					origin.shift(offsetVector);

					const bullet = new Bullet(player.id, player.colorId,
						origin, velocity, this.config.bulletType);
					bulletList.push(bullet);
				}
				break;

				// 3 bullets in triangle
			case GunConfig.TRIPLE :
				for (let i = 0; i < 3; ++i) {
						// Orientation of velocity in triangle
					const velocity = new Vector(
						Bullet.SPEED,
						player.hitbox.orientation + i * (Math.PI2 / 3));
						// Origin from the ship center
					const origin = player.hitbox.cloneOrigin();

					const bullet = new Bullet(player.id, player.colorId,
						origin, velocity, this.config.bulletType);
					bulletList.push(bullet);
				}
				break;

				// 3 bullets in line
			case GunConfig.LASER : 
				for (let i = 0; i < 3; ++i) {
						// Increase speed to make a line of bullets
					const velocity = new Vector(
						(1 + i/2) * Bullet.SPEED,
						player.hitbox.orientation);
						// Origin from the ship center
					const origin = player.hitbox.cloneOrigin();

					const bullet = new Bullet(player.id, player.colorId,
						origin, velocity, this.config.bulletType);
					bulletList.push(bullet);
				}
				break;

				// 3 bullets in arc
			case GunConfig.WAVE :
				// for (var i = -35; i <= 36; i++) {
				for (let i = -1; i <= 1; ++i) {
						// Orientation to make an arc
					const velocity = new Vector(
						Bullet.SPEED,
						player.hitbox.orientation + i*Math.PI/36);

						// Origin from the ship center
					const origin = player.hitbox.cloneOrigin();

					const bullet = new Bullet(player.id, player.colorId,
						origin, velocity, this.config.bulletType);
					bulletList.push(bullet);
				}		
				break;
		}
		return bulletList;
	}
}
