'use strict';

require('./Entity.js');
require('./Polygon.js');

module.exports = global.Player = class Player extends Entity {

		// - colorId : UTility.RED
	constructor (colorId) {
		super(Player.BOUND, Player.HITBOX.clone());

		this.gun	= new Gun();
		this.input 	= new Input();

		// this.tab_animation = new AnimationTab();
		this.speedMax 	= Player.SPEED_MAX;
		this.drag 		= Player.DRAG;

		this.colorId = colorId;
	}
		// Add force according to keys
		// - kPos & kNeg : codes of keys
		// 		- kPos : negatives coordinates (left or up)
		// 		- kNeg : positives coordinates (right or down)
		// - angle : radian (0 for vertical or PI/2 for horizontal)
	addInputForce (kPos, kNeg, angle) {
		try {
			const aPos = this.input.keyMap.get(kPos).isActive;
			const aNeg = this.input.keyMap.get(kNeg).isActive;
				// Determinate direction coef (-1 or 1)
			let directionCoef = aNeg - aPos;
			if (directionCoef) {
				directionCoef *= Player.ACCELERATION;
				const direction = (directionCoef > 0) ? angle : angle + Math.PI;
				const force = new Vector(Math.abs(directionCoef), direction);
				this.addForce(force);
			}
		} catch (e) {
			console.log(this.id);
			// console.log(kPos)
			// console.log(this.input.keyMap);
		}
	}
		// Return an array of bullet according to the gun
	shoot () {
		return this.gun.shoot(this);
	}

	//--- Tangible
		// Update position then gun
	//@Override
	stateUpdate (dt) {
		super.stateUpdate(dt);
		this.gun.update(dt);
	}
		// Adding input force then update entity speed
	// @Override
	velocityUpdate (dt) {
		this.addInputForce(	// x
			this.input.keyCode.left, this.input.keyCode.right, 0);
		this.addInputForce(	// y
			this.input.keyCode.up, this.input.keyCode.down, Math.PI_2);
		super.velocityUpdate(dt);
	}
		// Update the ship position
	// @Override
	positionUpdate (dt) {
		super.positionUpdate(dt);
		this.input.cursor.shift(this.deltaVelocity);
			// TODO create animation update function
		// this.tab_animation.translate(this.vector);
	}
		// Apply rotation force to rotation the ship
	// @Override
	orientationUpdate (dt) {
			// If cursor is active, set instantly
		if (this.input.cursor.isActive) {
			this.setOrientation(this.hitbox.angle(this.input.cursor));
		}
			// Else rotate at Player.ROTATION radian per second
		else {
			let sign = 
				this.input.keyMap.get(this.input.keyCode.r_right).isActive
				- this.input.keyMap.get(this.input.keyCode.r_left).isActive;
			this.rotate(sign * Player.ROTATION * dt);
		}
	}
	//--- Drawable
	draw (view, dt) {
		this.drawPhysic(view);

			// Draw a ship
		if (View.DRAW_VIEW) {
			if (View.DRAW_IMAGE) {
					// Save then move the context for tileset image drawing
				view.ctx.save();
				view.ctx.translate(
					this.ghost.hitbox.x + view.x, this.ghost.hitbox.y + view.y);
				view.ctx.rotate(this.hitbox.orientation);
					// Draw the ship according to tileset bonus
				const xTileset = 34*this.gun.configId;
				const yTileset = 33*this.colorId;
				if (this.gun.configId === GunConfig.TRIPLE) {
					for (let i = 0; i < 3; i++) {
						view.ctx.drawImage(Utility.SHIP_TILESET,
							xTileset, yTileset,
							33, 33, -10, -16, 33, 33);
						view.ctx.rotate(Math.PI2 / 3);
					}
				} else {
					view.ctx.drawImage(Utility.SHIP_TILESET, xTileset, yTileset,
						33, 33, -16, -16, 33, 33);
				}
					// Restore context
				view.ctx.restore();
			} else {
				// this.ghost.hitbox.draw(view, 'red');
				// MAYBE real color (also for bullets) but less visibility
				this.ghost.hitbox.draw(view, Utility.getRgbaColor(this.colorId, 100));
			}
		}
			// Cursor
		if (View.DRAW_HITBOX && View.DRAW_TRACE === undefined) {
			this.input.cursor.draw(view, 'cyan');
		}

			// Cooldown bonus jauge
		if (!this.gun.isNormal()) {
			const angle = Math.PI - Math.PI*this.gun.timeConfig/Gun.TIME_CONFIG;
			const shift = (this.gun.configId === GunConfig.TRIPLE) ? 0 : -4;
			const vector = new Vector(shift, this.hitbox.orientation);
			view.ctx.beginPath();
			view.ctx.arc(
				this.ghost.hitbox.x + view.x + vector.x,
				this.ghost.hitbox.y + view.y + vector.y, 
				Player.BOUND,
				this.hitbox.orientation + angle,
				this.hitbox.orientation - angle);
			view.ctx.lineWidth = 3;
			view.ctx.strokeStyle = Utility.getRgbaColor(this.colorId, 50);
			view.ctx.stroke();
		}

		this.ghost.velocity.draw(view, this.ghost.hitbox);
	}
}