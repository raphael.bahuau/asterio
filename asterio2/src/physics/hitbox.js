import Point from "./point";
import Circle from "./circle";
import Polygon from "./polygon";

const isCircle = (hitbox) => hitbox.radius !== undefined;

const move = (hitbox, vector) =>
  (isCircle(hitbox) ? Circle : Polygon).move(hitbox, vector);

const rotate = (hitbox, angle) =>
  isCircle(hitbox) ? hitbox : Polygon.rotate(hitbox, angle);

// TRUE if the point is in the hitbox
const contains = (hitbox, point) => {
  // Circle
  if (isCircle(hitbox)) {
    return Point.near(hitbox, point, this.radius);
  }

  // Polygon
  for (let i = 0, l = hitbox.points.length; i < l; i++) {
    const i2 = (i + 1) % l;
    // Vector between 2 points of the polygon
    const v1 = Point.sub(hitbox.points[i2], hitbox.points[i]);
    // Vector between the point and a point of the polygon
    const v2 = Point.sub(point, hitbox.points[i]);
    // Calculate if the point is on the right of the segment formed
    //   by the 2 points of the vector 1
    // If it's true, the point is obviously outside
    if (v1.x * v2.y - v1.y * v2.x < 0) {
      return false;
    }
  }
  // Else if the point is always on the left, he's inside
  return true;
};

// TRUE if the 2 hitbox are touching each other
//    (at least 1 point is in the other)
// MAYBE for circle collision return the true collision point
//    (calculate the perpendicular of the vector of the contains function)
const collides = (h1, h2) =>
  isCircle(h1)
    ? Point.near(h1, h2, h1.radius + h2.radius)
    : h1.points.some((p) => contains(h2, p));

/* --------------------------------- EXPORT --------------------------------- */

const Hitbox = {
  isCircle,
  move,
  rotate,
  contains,
  collides,
};

export default Hitbox;
