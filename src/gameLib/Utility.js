'use strict';

module.exports = global.Utility = {
		// Return a int between min & max include
		// - if no args => 0 or 1
		// - if only min => 0 ~ min
	random : function (min = 1, max = 0) {
		const min2 = Math.ceil(Math.min(min, max));
		const max2 = Math.floor(Math.max(min, max));
		return Math.floor(Math.random()*(max2 - min2 + 1)) + min2;
	},

		// Return degre angle in radian
	toRad : function (deg) {
		return deg*Math.PI/180;
	},

		// Return radian angle in degre
	toDeg : function (rad) {
		return rad*180/Math.PI;
	},

		// To debug
	// lock : false,

		// Return a rgba string
		// - colorId : Utility.RED
		// - shade : (255 = white, 0 = black)
		// - alphaCoef : tranparency
	getRgbaColor : function (colorId, shade, alphaCoef = 1) {
		const rgb = Utility.RGB_MAP.get(colorId);
		const shadeRgb = rgb.map(v => v + shade);
		shadeRgb.push(alphaCoef);
		return 'rgba(' + shadeRgb.join(',') + ')';
	},

	randomInArray(a) {
		return a[Math.floor(Math.random()*a.length)];
	},

	randomInMap(m) {
		return m.get([...m.keys()][Math.floor(Math.random()*m.size)])
	}
}