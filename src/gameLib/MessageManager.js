'use strict';

	// Message manager
	// TODO optimise message size : concat string array in one string
module.exports = global.MessageManager = class MessageManager {

	constructor () {
			// To send at the next iteration (ups)
		this.toSend	= [];

			// To handle at the next iteration (ups)
		this.toHandle = [];

			// In process
		this.inProcess = {};
		this.inProcess[Message.Type.INPUT] = [];
		this.inProcess[Message.Type.STATE] = {};
		this.inProcess[Message.Type.STATE][Message.State.GENERATE	] = [];
		this.inProcess[Message.Type.STATE][Message.State.UPDATE		] = [];
		this.inProcess[Message.Type.STATE][Message.State.REMOVE		] = [];
	}

		// Sort message from handle to process, and clear the handle list
		// MAYBE pass only message.args in list inProcess to simplify after ?
	setProcess () {
	// setProcess (gameTime) {
		for (let i = 0; i < this.toHandle.length; i++) {
			const message = this.toHandle[i];
				// If wait more than 50ms, prepare it to be processed
			// const ms = gameTime - message.args.t + 0;
			// if (ms > 0) {
				// if (message.type !== Message.Type.STATE) {
				// 	console.log('TIME : ' + gameTime + ' / ' + ms);
				// }
				if (message.type === Message.Type.INPUT) {
					this.inProcess[message.type].push(message);
				} else {
					this.inProcess[message.type][message.subType].push(message);
				}
					// Remove message from toHandle list
				// this.toHandle.splice(i--, 1);
		}
		this.toHandle = [];
	}
}