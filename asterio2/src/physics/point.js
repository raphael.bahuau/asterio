// - x, y: 2D coordinates
const get = (x, y) => ({ x, y });

// - return: radian
const angle = (p1, p2) => Math.atan2(p2.y - p1.y, p2.x - p1.x);

const range = (p1, p2) =>
  Math.sqrt(Math.pow(p2.x - p1.x, 2) + Math.pow(p2.y - p1.y, 2));

const near = (p1, p2, range) => Point.range(p1, p2) <= range;

const sum = (p1, p2) => ({ x: p1.x + p2.x, y: p1.y + p2.y });

const sub = (p1, p2) => ({ x: p1.x - p2.x, y: p1.y - p2.y });

// Rotate around the origin
// - origin: Point
// - angle: radian
const pivot = (p, origin, angle) => {
  const cos = Math.cos(angle);
  const sin = Math.sin(angle);
  const x = p.x - origin.x;
  const y = p.y - origin.y;
  return {
    x: cos * x - sin * y + origin.x,
    y: sin * x + cos * y + origin.y,
  };
};

/* --------------------------------- EXPORT --------------------------------- */

const Point = {
  ZERO: { x: 0, y: 0 },
  get,
  angle,
  range,
  near,
  sum,
  sub,
  pivot,
};

export default Point;
